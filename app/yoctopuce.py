#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from yocto_api import *
from yocto_lightsensor import *
errmsg = YRefParam()
if (YAPI.RegisterHub("usb", errmsg)!=YAPI.SUCCESS):
  print(errmsg.value)
  sys.exit(1)
sensor = YLightSensor.FindLightSensor("LIGHTMK4-272DC6.lightSensor")
#sensor = YLightSensor.FirstLightSensor()
if (sensor==None):
  print("No light sensor found")
  sys.exit(1)
while (True):
  if  sensor.isOnline():
    print(str(sensor.get_currentValue())+" "+sensor.get_unit())
  else:
    print("sensor went offline")
  YAPI.Sleep(1000)
