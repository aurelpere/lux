from django.shortcuts import render, redirect
from lux.forms import DatesForm
from lux.models import Lux
#from django.contrib.gis.geos import Point
from rest_framework import viewsets
from lux.serializer import LuxSerializer
from django.urls import reverse
import requests
import dateutil.parser
import datetime
import dateparser
class LuxViewSet(viewsets.ModelViewSet):
    queryset = Lux.objects.filter(time__gte=datetime.datetime.now()-datetime.timedelta(days=3)).order_by('-time')  # pylint: disable=E1101
    serializer_class = LuxSerializer

def index(request):
    ##########verifier orderby -time pour datetime field
    l=Lux.objects.all().order_by('-time').first()
    return render(request, 'index.html', {'value': l.lux, 'timevalue':l.time}) #{'value': l}

def select(request):
    if request.method == "POST":
        form = DatesForm(request.POST)
        if form.is_valid():
            print("form is valid")
            try:
                ##########verifier parsing de date 
                mindate = dateparser.parse(request.POST.get('mindate'),languages=['fr']).date()
                maxdate = dateparser.parse(request.POST.get('maxdate'),languages=['fr']).date()
                #for direct db query
                ##########verifier lt et gt pour datetime
                r = Lux.objects.filter(time__lte=maxdate).filter(time__gte=mindate)
                print(r)
                return render(request, 'show.html', {'luxs': r})
            except Exception as e:  # pylint: disable=W0703
                print(e)
        else:
            print("form is not valid")
            print(form.errors.as_data())
    else:
        form = DatesForm()
    return render(request, 'select.html', {'form': form})

def show(request):
    #peaks = Peak.objects.all() for direct query on the db
    r = requests.get(request.build_absolute_uri(location=reverse('lux-list')),
                     timeout=10).json()
    print(r)
    print(type(r))
    return render(request, "show.html", {'luxs': r})


