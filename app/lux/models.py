from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone

# Create your models here.
class Lux(models.Model):
    ##########verifier DateTimeField
    time = models.DateTimeField(default=timezone.localtime,unique=True)
    lux = models.IntegerField(
        validators=[RegexValidator(regex=r"\d{1,6}")])
    class Meta:
        db_table = "lux_table"
