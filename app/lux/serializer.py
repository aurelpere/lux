from rest_framework import serializers
from lux.models import Lux


# Create your models here.
class LuxSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Lux
        fields = ('id', 'time', 'lux')
