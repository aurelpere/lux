import importlib, inspect
import os
from core.settings import BASE_DIR
class Command(BaseCommand):
    help = "Gets a list of all classes declared in models.py"

    def handle(self, *args, **options):
        for name, cls in inspect.getmembers(importlib.__import__(os.path.join(BASE_DIR, 'projects/models.py')), inspect.isclass):
            print(name)
            self.stdout.write(name)
