#!/bin/bash -l
from django.core.management.base import BaseCommand, CommandError
from datetime import timedelta
from django.utils import timezone
import datetime
from django.conf import settings
from django.core.mail import send_mail, mail_admins
from django.urls import reverse
#from core.settings import BASE_DIR
from django.db import connection
import json
import os
from yocto_api import *
from yocto_lightsensor import *
from datetime import *
from lux.models import Lux
from zoneinfo import ZoneInfo
class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lastCallback=datetime.now(ZoneInfo('Europe/Paris'))
        self.lux=0
    def handle(self, *args, **options):
        def log(msg,onscreenAsWell =False ):
            if (onscreenAsWell): 
                print(msg)
            logfile =  "main.log"
            line=  str(datetime.now(ZoneInfo('Europe/Paris'))) + " "+msg

            with open("main.log", "a") as myfile:
                myfile.write(line+"\n")
            if os.path.getsize(logfile) > 32*1024:
                with open(logfile, 'r') as fin:
                    lines = fin.read().splitlines(True)
                with open(logfile, 'w') as fout:
                    start = int(len(lines)/10)
                    fout.writelines(lines[start:])

        self.lastCallback=datetime.now(ZoneInfo('Europe/Paris'))
        unit = ""

        def valueCallback(source,measure):
            print (str(measure.get_averageValue())+"lux")
            self.lastCallback=datetime.now(ZoneInfo('Europe/Paris'))
            print(str(self.lastCallback))
            self.lux=int(measure.get_averageValue())

        log("**** Hello, starting application...",False)

        YAPI.RegisterLogFunction(log)

        errmsg = YRefParam()
        if (YAPI.RegisterHub("usb", errmsg)!=YAPI.SUCCESS):
            log(errmsg.value,True)
            sys.exit(1)

        sensor =  YLightSensor.FirstLightSensor()
        if (sensor==None):
            log("No light sensor found",True)
            sys.exit(1)

        unit = sensor.get_unit();
        sensor.set_reportFrequency("1/s")#1/s==per second, 1/m==per minute, 1/h==per hour
        sensor.get_module().saveToFlash()
        sensor.registerTimedReportCallback(valueCallback)

        errorCount = 0
        count=0
        while (True):
            try :
                #self.lastCallback=datetime.now()
                YAPI.Sleep(1000)
                YAPI.UpdateDeviceList()
                #print(self.lastCallback)
                #print(datetime.now())
                if (datetime.now(ZoneInfo('Europe/Paris')) - self.lastCallback).total_seconds()>=2:
                    log("device is offline",True)
                errorCount = 0
                count+=1
                if count%60==0:
                    Lux.objects.create(time=self.lastCallback,lux=self.lux)


            except Exception as e:
                log("An exception occured : "+str(e),True)
                errorCount = errorCount + 1
                if (errorCount>3):
                    log("Giving up..")
                    sys.exit(1)
