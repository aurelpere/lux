django server for yoctopuce yoctolight v4 (https://www.yoctopuce.com/EN/products/usb-environmental-sensors/yocto-light-v4)
light measurements are automatically registered to the database every 60s.

# utilisation 
<br>

`git clone https://gitlab.com/aurelpere/lux.git`
>download repository
<br>

`cd lux && docker-compose up`
>deploy django server locally (docker and docker compose must be installed)
<br>

`localhost:80`
>enter this adress in your browser to get the website (django frontend). you can retrieve the list of lux measurements, and select them based on dates
<br>

